# Préstamo de Bicicletas
## _Objetivo_
El objetivo del módulo es gestionar una "flota" de bicicletas así como su estado o su historial de préstamos, a fin de facilitar el control de las mismas.
## _Estructura_
La estructura del módulo se parte en tres vistas:
- **Bicis**:
    En la vista general se pueden ver las bicicletas ya creadas en **Kamban** donde se pueden organizar por columnas según su tipo.
    
    Incluye un formulario donde se puede añadir o modificar bicicletas, cambiarla entre los posibles estados y asignarla a un "Usuario" que hereda de res.partner. Haciendo uso de la **herencia** podemos reutilizar información útil sobre cada cliente.


- **Bici Tipos**:
    Por defecto hay 6 tipos de bicicletas ya creados donde "Bicicleta de Montaña" es padre de tres de ellos.


- **Bici Loans**:
    Incorpora una vista **Calendar** donde consultar y guardar informacion de todos los prestamos.


## _Autor_
_Jacobo Fernández Barreiro_
