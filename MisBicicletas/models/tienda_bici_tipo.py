# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError


class BiciTipo(models.Model):
    _name = 'tienda.bici.tipo'

    _parent_store = True
    _parent_name = "parent_id"  # optional if field is 'parent_id'

    name = fields.Char('Tipo')
    description = fields.Text('Descripción')
    parent_id = fields.Many2one(
        'tienda.bici.tipo',
        string='Tipo padre',
        ondelete='restrict',
        index=True
    )
    child_ids = fields.One2many(
        'tienda.bici.tipo', 'parent_id',
        string='Subtipos')
    parent_path = fields.Char(index=True)

    @api.constrains('parent_id')
    def _check_hierarchy(self):
        if not self._check_recursion():
            raise models.ValidationError('Error! No puedes hacer tipos recursivos.')