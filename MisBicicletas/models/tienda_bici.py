# -*- coding: utf-8 -*-
import logging

from odoo import models, fields, api
from odoo.exceptions import UserError
from odoo.tools.translate import _
from datetime import datetime, timedelta


logger = logging.getLogger(__name__)


class TiendaBici(models.Model):
    _name = 'tienda.bici'
    _description = 'Tienda bici'

    name = fields.Char('Titulo', required=True)
    date_release = fields.Date('Fecha de Salida')
    date_updated = fields.Datetime('Ultima actualización', copy=False)
    tipo_id = fields.Many2one('tienda.bici.tipo', string='Tipo')
    state = fields.Selection([
        ('draft', 'No Disponible'),
        ('available', 'Disponible'),
        ('borrowed', 'Prestado'),
        ('lost', 'Perdido')],
        'State', default="draft")

    is_lent = fields.Boolean('Lent', compute='check_lent', default=False)
    loan_ids = fields.One2many('tienda.loan', inverse_name='bici_id')
    bici_image = fields.Binary('Imagen')

    @api.multi
    def check_lent(self):
        for bici in self:
            domain = ['&',('bici_id.id', '=', bici .id), ('date_end', '>=', datetime.now())]
            bici.is_lent = self.env['tienda.loan'].search(domain, count=True) > 0

    @api.model
    def is_allowed_transition(self, old_state, new_state):
        allowed = [('draft', 'available'),
                   ('available', 'borrowed'),
                   ('borrowed', 'available'),
                   ('available', 'lost'),
                   ('borrowed', 'lost'),
                   ('lost', 'available')]
        return (old_state, new_state) in allowed

    @api.multi
    def change_state(self, new_state):
        for bici in self:
            if bici.is_allowed_transition(bici.state, new_state):
                bici.state = new_state
            else:
                message = _('No se puede cambiar de %s a %s') % (bici.state, new_state)
                raise UserError(message)

    def make_available(self):
        self.change_state('available')

    def make_borrowed(self):
        self.change_state('borrowed')

    def make_lost(self):
        self.change_state('lost')

    def create_tipos(self):
        if self.env['tienda.bici.tipo'].search([('name', '=' ,'BMX')]).name == False:
            categ1 = {
                'name': 'Bicicleta de Enduro',
                'description': 'Esta modalidad de bici todo terreno consigue atravesar montañas por los lugares más complicados y a gran velocidad . A diferencia del Descenso, también existen subidas de partes de la montaña.'
            }
            categ2 = {
                'name': 'BMX',
                'description': 'Las BMX utilizan cuadros pequeños y resistentes, ideales para saltos y acrobacias'
            }
            categ3 = {
                'name': 'Bicicleta de Descenso',
                'description': 'Construida para bajar la montaña a altas velocidades y soportar el castigo extremo. Están equipadas con la más alta tecnología, enfoca primordialmente en una doble suspensión que le brinde un desplazamiento capaz de absorber impactos a alta velocidad.'
            }
            categ4 = {
                'name': 'Bicicleta de Cross Country',
                'description': 'También llamada XC, esta modalidad consiste en atravesar la montaña a alta velocidad, con la diferencia de que se circula prácticamente todo por pistas llanas con puntuales complicaciones.'
            }
            categ5 = {
                'name': 'Bicicleta utilitaria',
                'description': 'Al ser utilizadas para mover biciga, esta familia de bicis utilizan cuadros sumamente fuertes, con un buen sistema de frenos (frenos de disco, tambor, son comunes) y una relación de velocidades adecuada para mover el peso extra.'
            }
            parent_tipo_val = {
                'name': 'Bicicleta de montaña',
                'descripcion': 'Las bicicletas de montaña (también conocidas como MTB) están diseñadas para «fuera de ruta» por lo que son bastante resistentes.',
                'child_ids': [
                    (0, 0, categ1),
                    (0, 0, categ3),
                    (0, 0, categ4),
                ]
            }

            record = self.env['tienda.bici.tipo'].create(parent_tipo_val)
            record = self.env['tienda.bici.tipo'].create(categ2)
            record = self.env['tienda.bici.tipo'].create(categ5)
        return True

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(TiendaBici, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        self.create_tipos()
        return res

    @api.multi
    def change_update_date(self):
        self.ensure_one()
        self.date_updated = fields.Datetime.now()

    @api.multi
    def find_bici(self):
        domain = [
            '|',
                '&', ('name', 'ilike', 'Bici Name'),
                     ('tipo_id.name', '=', 'Tipo Name'),
                '&', ('name', 'ilike', 'Bici Name 2'),
                     ('tipo_id.name', '=', 'Tipo Name 2')
        ]
        bicis = self.search(domain)
        logger.info('bicis found: %s', bicis)
        return True

    @api.model
    def get_all_tienda_members(self):
        tienda_member_model = self.env['tienda.member']  # This is an empty recordset of model tienda.member
        return tienda_member_model.search([])



class TiendaMember(models.Model):
    _name = 'tienda.member'
    _inherits = {'res.partner': 'partner_id'}

    partner_id = fields.Many2one('res.partner', ondelete='cascade')
    date_start = fields.Date('Miembro desde')
    date_end = fields.Date('Fecha de fin')
    member_number = fields.Char()
    date_of_birth = fields.Date('Fecha de nacimiento')

class TiendaLoan(models.Model):
    _name = 'tienda.loan'
    _description = 'Tienda Loan'
    _rec_name = 'bici_id'
    _order = 'date_end desc'
    bici_id = fields.Many2one('tienda.bici', required=True)
    member_id = fields.Many2one('tienda.member', string='Usuario', required=True)
    date_start = fields.Date('Comienzo préstamo', default=lambda *a:datetime.now().strftime('%Y-%m-%d'))
    date_end = fields.Date('Fín de préstamo', default=lambda *a:(datetime.now() + timedelta(days=(1))).strftime('%Y-%m-%d'))

    @api.constrains('bici_id')
    def _check_bici_id(self):
        for loan in self:
            bici = loan.bici_id
            domain = ['&',('bici_id.id', '=', bici.id), ('date_end', '>=', datetime.now())]
            bici.is_lent = self.search(domain, count=True) > 1 
          
    @api.constrains('date_end', 'date_start')
    def _check_dates(self):
        for loan in self:
            if loan.date_start > loan.date_end:
                raise models.ValidationError('Fecha de fin no puede ser anterior a la fecha de comienzo!')
