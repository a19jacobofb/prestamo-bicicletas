# -*- coding: utf-8 -*-
{
    'name': "Mis Bicicletas",  # Module title
    'summary': "Control de bicicletas",  # Module subtitle phrase
    'description': """Long description""",  # You can also rst format
    'author': "Jacobo Fernández",
    'website': "http://www.example.com",
    'category': 'Uncategorized',
    'version': '12.0.1',
    'depends': ['base'],
    # This data files will be loaded at the installation (commented becaues file is not added in this example)
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/tienda_bici.xml',
        'views/tienda_bici_tipo.xml',
        'views/tienda_loan.xml'
    ],
    # This demo data files will be loaded if db initialize with demo data (commented becaues file is not added in this example)
    # 'demo': [
    #     'demo.xml'
    # ],
}
